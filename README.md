# Grenv - gradle versions manager

## Install

Run command:

```
curl -L -O -s https://bitbucket.org/demand/grenv/downloads/grenv-install.zip && unzip -p grenv-install.zip src/install.sh | bash -s -- -d
```

or

1. Download [archive](https://bitbucket.org/demand/grenv/downloads/grenv-install.zip)
2. Unpack it
3. Run `src/install.sh`

## Usage

`grenv info`

  - check for last version and show actual JVM for gradle 

`grenv list`

  - show list of installed gradle versions

`grenv switch {version}`

  - switch to required version

