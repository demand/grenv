#!/usr/bin/env bash

do_install() {
    local reset_color="\033[0m"
    local bold_color="\033[93m"
    local link_color="\033[34m"

    local script_file="$1"
    local script_dir=$(dirname "$script_file")

    local script_file_name="$(basename $script_file)"
    local zip_filename="grenv-install.zip"

    local install_dir="/usr/local/lib/gradle/env"
    local command_in_path="/usr/local/bin"
    local bash_completion_dir="/usr/local/etc/bash_completion.d"

    if [[ -f "$install_dir/grenv" && -f "$command_in_path/grenv" ]]; then
        echo -e "Gradle versions manager has been already installed in ${link_color}$install_dir$reset_color"
        echo -e "Use ${bold_color}grenv$reset_color command" 
    else
        if [ $script_file_name == "bash" ]; then
            # install from zip
            zip_path="$script_dir/$zip_filename"
            mkdir -p "$install_dir"
            unzip -qj $zip_path "src/grenv" -d $install_dir
            ln -sf "$install_dir/grenv" $command_in_path
            if [ -d $bash_completion_dir ]; then
                mkdir -p "$install_dir/bash_completion"
                unzip -qj $zip_path "src/bash_completion/grenv" -d "$install_dir/bash_completion"
                ln -sf "$install_dir/bash_completion/grenv" "$bash_completion_dir/grenv"
            fi    
            if [ "$2" == "-d" ]; then
                rm $zip_path
            fi    
        else
            # install from folder
            folder_path="$(dirname $script_dir)/$zip_filename"
            mkdir -p "$install_dir"
            cp "$script_dir/grenv" $install_dir
            ln -sf "$install_dir/grenv" $command_in_path
            if [ -d $bash_completion_dir ]; then
                mkdir -p "$install_dir/bash_completion"
                cp "$script_dir/bash_completion/grenv" "$install_dir/bash_completion"
                ln -sf "$install_dir/bash_completion/grenv" "$bash_completion_dir/grenv"
            fi    
        fi

        echo "Gradle versions manager has been installed successfully"
        echo -e "Use ${bold_color}grenv$reset_color command" 
    fi    
}

do_install $(realpath "$0") $1