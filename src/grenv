#!/usr/bin/env bash

script_file=$(realpath "$0")
script_dir=$(dirname "$script_file")
grenv_home_dir=$(dirname "$script_dir")

mark_version_prefix="\033[94m  --> "
void_version_prefix="      "
reset_color="\033[0m"
err_color="\033[91m"
bold_color="\033[93m"
title_color="\033[92m"
link_color="\033[34m"

gradle_in_path="/usr/local/bin/gradle"

echo_title() {
    echo -e "${title_color}Gradle versions manager\033[0m"
    echo
}

contains_element () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

fill_versions() {
    bundle_folders=($(find $grenv_home_dir -type d -mindepth 1 -maxdepth 1))

    local cur_gradle_in_path
    local cur_gradle_home
    cur_gradle_in_path="$(which gradle)"
    cur_gradle_home=""

    if [ "$cur_gradle_in_path" != "" ]; then
        cur_gradle_command_link=$(readlink "$cur_gradle_in_path")
        if [[ "$cur_gradle_command_link" =~ \/bin\/gradle$ ]]; then
            cur_gradle_home=$(dirname $(dirname $cur_gradle_command_link))
        fi
    fi

    versions=()
    pure_versions=()
    for ff in ${bundle_folders[*]}; do
        if [ "$script_dir" != "$ff" ]; then
            ver="${ff##*/}"
            pure_versions+=($ver)
            if [ "$cur_gradle_home" != "" ] && [ "$ff" == "$cur_gradle_home" ]; then
                versions+=("$mark_version_prefix$ver$reset_color")
            else 
                versions+=("$void_version_prefix$ver$reset_color")
            fi    
        fi
    done
}

switch_to_version() {
    desired_ver=$1
    gradle_ver_bin="$grenv_home_dir/$desired_ver/bin/gradle"

    if [ -f "$gradle_ver_bin" ]; then
        command="ln -sf $gradle_ver_bin $gradle_in_path"

        eval $command
        echo "Switched to $desired_ver"
        res=0
    else
        echo -e "${err_color}Version $desired_ver is not registered$reset_color"
        res=1
    fi
}

check_updates() {
    updates_command="curl https://services.gradle.org/distributions/ 2>/dev/null | grep -E '/distributions/gradle-[0-9.]+-bin\.zip\"' | head -10 | sed -E 's/<a[[:space:]]href=\"([^\"]+).+/\1/'"

    top10_releases=($(eval "$updates_command"))

    if [ ${#top10_releases[@]} -ne 0 ]; then
        last_release=$(echo "${top10_releases[0]}" | sed -E "s/.+-([^-]+)-bin\.zip$/\1/")
        
        contains_element "$last_release" "${pure_versions[@]}"
        if [ $? -eq 0 ] ; then
            echo -e "You already have last release version $bold_color$last_release$reset_color"
        else
            echo -e "New release $bold_color$last_release$reset_color is available at ${link_color}https://services.gradle.org/distributions/$reset_color"
        fi    
    fi
}

if [ $# -eq 0 ]; then
    echo "No args."
    echo "Use one of: list, purelist, switch, info, home"
    exit 1
elif [ "$1" == "home" ]; then
    echo "$grenv_home_dir"
    exit 0
elif [ "$1" == "list" ]; then
    fill_versions
    echo_title
    echo "Registered versions:"
    for ver in "${versions[@]}"; do
        echo -e "$ver"
    done
    exit 0
elif [ "$1" == "purelist" ]; then
    fill_versions
    for ver in "${pure_versions[@]}"; do
        echo -e "$ver"
    done
    exit 0
elif [ "$1" == "info" ]; then
    echo_title

    fill_versions

    check_updates
    if type gradle >/dev/null 2>&1; then
        java_ver=$(gradle --version | grep 'JVM:' | sed -E 's/JVM:[[:space:]]+(.+)/\1/g')
        echo
        echo "Current gradle uses JVM $java_ver"
    else
        echo
        echo -e "${err_color}No gradle command in PATH$reset_color"
    fi    
    exit 0
elif [ "$1" == "switch" ]; then
    echo_title
    if [ ! -z $2 ]; then
        unset res
        switch_to_version $2
        exit $res
    else
        echo -e "${err_color}Version has to be specified$reset_color"

        echo "Available versions:"

        fill_versions
        for ver in "${versions[@]}"; do
            echo -e "$ver"
        done
    fi    
    exit 0
else
    echo_title
    echo "Unknown command $1"
    exit 1
fi
