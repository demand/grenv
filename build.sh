#!/usr/bin/env bash

run_build() {
    local script_dir=$(dirname "$1")
    local to_zip_files=()
    cd $script_dir
    to_zip_files=($(find "./src" -type f))
    to_zip_files+=("./README.md")

    local files_list=$(printf "\n%s" "${to_zip_files[@]}")

    rm -r "./build" 2>/dev/null
    mkdir "./build"
    echo "$files_list" | zip -q@ ./build/grenv-install.zip
}

run_build $(realpath "$0")